class ItemsController < ApplicationController

  def index
    @items = Item.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @items }
    end
  end

  def show
    @item = Item.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @item }
    end
  end

  def new
    @item = Item.new
    #@persons = Person.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @item }
    end
  end

  def edit
    @item = Item.find(params[:id])
    #@persons = Person.all
  end

  def create
    #render plain: params[:item].inspect
    @item = Item.new(params[:item])

    ## build a photo and pass it into a block to set other attributes
    #@item = Item.new(params[:item]) do |t|
    #  if params[:item][:poster]
    #    uploaded_io = params[:item][:poster]
    #    File.open(Rails.root.join('public', 'uploads', uploaded_io.original_filename), 'wb') do |file|
    #      file.write(uploaded_io.tempfile)
    #    end
    #    #t.poster    = params[:item][:poster].read
    #    #t.poster  = 'test'# uploaded_io.original_filename
    #    #t.mime_type = params[:item][:poster].content_type
    #  end
    #end
    #@item.poster = "http://ia.media-imdb.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_SY317_CR1,0,214,317_AL_.jpg"
    #@item.poster = params[:item][:poster].original_filename
    ##@item.poster = Rails.root.join('public', 'uploads', params[:item][:poster].original_filename)
    ##http://ia.media-imdb.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_SY317_CR1,0,214,317_AL_.jpg
    #

    respond_to do |format|
      if @item.save
        #format.html { redirect_to @item, notice: 'Элемент обновлен.' }
        format.html { redirect_to edit_item_url(@item), notice: 'Элемент обновлен.' }
        format.json { render json: @item, status: :created, location: @item }
      else
        format.html { render action: 'new' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @item = Item.find(params[:id])

    #@item.persons = self.filter_persons(params[:persons])

    respond_to do |format|
      if @item.update_attributes(params[:item])
        #format.html { redirect_to @item, notice: 'Элемент обновлен.' }
        format.html { redirect_to edit_item_url(@item), notice: 'Элемент обновлен.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @item.errors, status: :unprocessable_entity }
      end
    end
  end

  def filter_persons(persons)

    @filtered = persons.each do |p|
      if p != ''
        return p
      end
    end

    return @filtered
  end

  def destroy
    @item = Item.find(params[:id])
    @item.destroy

    respond_to do |format|
      format.html { redirect_to items_url }
      format.json { head :no_content }
    end
  end

end

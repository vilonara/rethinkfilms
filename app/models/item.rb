class Item
  include NoBrainer::Document

  # Defines the fields of the document visible to the application
  field :title
  field :title_original
  field :type, :default => "film"
  field :description
  field :year
  field :poster
  field :bg_image
  field :countries #, :type => Array
  field :persons
  field :companies

  #has_many :releases
  #has_many :poster
  has_many :persons
  #has_many :companies

  #serialize :persons, Array

  #<a class="th [radius]" href="#">
  #<img src="http://ia.media-imdb.com/images/M/MV5BMzA2NDkwODAwM15BMl5BanBnXkFtZTgwODk5MTgzMTE@._V1_SY317_CR1,0,214,317_AL_.jpg">
  #</a>
  #              <%= form_for @item do |f| %>
  #                  <%= item_form.file_field :poster %>
  #              <% end %>


  # Rails validations
  validates :title, :presence => true
  validates :title_original, :presence => true
  validates :description, :presence => true
  validates :year, :presence => true

  def get_persons(role)
    return Person.all #where(:role => role) #Person.where(:role => role)
  end

end

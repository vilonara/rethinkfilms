class Cast
  include NoBrainer::Document

  field :name
  field :character
  field :picture

  belongs_to :item

end

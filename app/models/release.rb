class Cast
  include NoBrainer::Document

  field :country
  field :date

  belongs_to :item

end
